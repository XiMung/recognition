import React, { useEffect, useRef, useState } from 'react'
import * as mobilenet from '@tensorflow-models/mobilenet';
import * as knnClassifier from '@tensorflow-models/knn-classifier';
import { initNotifications, notify } from '@mycv/f8-notification';
import * as tf from '@tensorflow/tfjs'
import { Howl } from 'howler';
import './App.css';
import soundURL from './assets/hey_sondn.mp3';

var sound = new Howl({
  src: [soundURL]
});


const NOT_HAND_LABEL = 'not_hand';
const HAND_LABEL = 'hand';
const TRAINING_TIME = 50;
const CONFIDENCES_TOP = 0.8;


function App() {
  const video = useRef();
  const mobilenetModule = useRef();
  const classifier = useRef();
  const canPlaySound = useRef(true);
  const [hand, setHand] = useState(false);



  const setupCamera = () => {
    return new Promise((resolve, reject) => {
      navigator.getUserMedia = navigator.getUserMedia ||
        navigator.webkitGetUserMedia ||
        navigator.mozGetUserMedia ||
        navigator.msGetUserMedia;

      if (navigator.getUserMedia) {
        navigator.getUserMedia(
          { video: true },
          stream => {
            video.current.srcObject = stream;
            video.current.addEventListener('loadeddata', resolve)
          },
          error => reject(error)
        )
      } else {
        console.log('hahahaha');
        reject();
      }
    })

  }

  const train = async lable => {
    console.log(lable);
    for (let i = 0; i < TRAINING_TIME; i++) {
      console.log(`Progress ${parseInt((i + 1) / TRAINING_TIME * 100)}%`);
      await training(lable);
    }
  }

  const training = lable => {
    return new Promise(async resolve => {
      const embedding = mobilenetModule.current.infer(
        video.current,
        true
      )
      classifier.current.addExample(embedding, lable);
      await sleep(100);
      resolve();
    })
  }

  const run = async () => {
    const embedding = mobilenetModule.current.infer(
      video.current,
      true
    )

    const result = await classifier.current.predictClass(embedding);

    if (result.label === HAND_LABEL && result.confidences[result.label] > CONFIDENCES_TOP) {
      console.log('have hand');
      if(canPlaySound.current === true) {
        canPlaySound.current = false;
        sound.play();
      }
      notify('Mụn đấy!', { body: 'Bạn vừa chạm tay vào mặt' });
      setHand(true);
    } else {
      console.log('have no hand');
      setHand(false);
    }

    await sleep(200);
    run();
  }

  const sleep = (ms = 0) => {
    return new Promise(resolve => setTimeout(resolve, ms))
  }

  const init = async () => {
    await setupCamera();

    classifier.current = knnClassifier.create();
    mobilenetModule.current = await mobilenet.load();
    console.log('khong cham tay len mat va bam train 1');
    initNotifications({ cooldown: 3000 });
  }

  useEffect(() => {
    init();

    sound.on('end', function(){
      canPlaySound.current = true;
    });

    // return () => {

    // }
  }, []);

  return (
    <div className={`main ${hand ? 'hand' : ''}`}>
      <video
        ref={video}
        className="video"
        autoPlay
      />

      <div className="control">
        <button className="btn" onClick={() => train(NOT_HAND_LABEL)} >Train 1</button>
        <button className="btn" onClick={() => train(HAND_LABEL)}>Train 2</button>
        <button className="btn" onClick={() => run()}>Run</button>
      </div>
    </div>
  );
}

export default App;
